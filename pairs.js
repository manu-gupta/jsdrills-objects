const pairs = (obj) => {
  if (!obj) return [];
  let keyValuePair = [];
  for (let key in obj) {
    keyValuePair.push([key, obj[key]]);
  }
  return keyValuePair;
};

module.exports = pairs;
