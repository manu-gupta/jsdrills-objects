const invert = (obj) => {
  if (!obj) return {};

  let invertedObject = {};
  for (let key in obj) {
    let newKey = obj[key];
    invertedObject[newKey] = key;
  }
  return invertedObject;
};

module.exports = invert;
