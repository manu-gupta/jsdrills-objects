let values = (obj) => {
  if (!obj) return [];
  let valuesArray = [];
  for (let key in obj) {
    valuesArray.push(obj[key]);
  }
  return valuesArray;
};

module.exports = values;
