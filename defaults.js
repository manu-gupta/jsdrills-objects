const defaults = (obj, defaultProps) => {
  if (!obj || !defaultProps) return {};

  for (let key in defaultProps) {
    if (!obj.hasOwnProperty(key)) {
      obj[key] = defaultProps[key];
    }
  }
  return obj;
};

module.exports = defaults;