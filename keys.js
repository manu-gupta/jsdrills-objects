const keys = (obj) => {
  if (!obj) return [];
  let keysArray = [];
  for (let key in obj) {
    keysArray.push(key);
  }
  return keysArray;
};

module.exports = keys;
