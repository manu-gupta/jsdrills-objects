const mapObject = (obj, callback) => {
  if (!obj) return {};

  let newObject = {};

  if (callback instanceof Function) {
    for (let key in obj) {
      let modifiedValue = callback(obj[key]);
      newObject[key] = modifiedValue;
    }
  }
  return newObject;
};

module.exports = mapObject;
